﻿float minelem(float2 v) {
    return min(v.x, v.y);
}

float minelem(float3 v) {
    return min(min(v.x, v.y), v.z);
}

float maxelem(float2 v) {
    return max(v.x, v.y);
}

float maxelem(float3 v) {
    return max(max(v.x, v.y), v.z);
}

bool in_box(float3 pos, float3 box_center, float3 box_extent) {
    return all(abs(pos - box_center) <= box_extent);
}

// Returns cube(volume) - ray intersection, t>=0.
bool isect_box(float3 org_raw, float3 dir, float3 box_center, float3 box_extent, out float3 pos) {
    if (in_box(org_raw, box_center, box_extent)) {
        pos = org_raw;
        return true;
    }

    // Coordinates where the AABB is centered at the origin.
    float3 org = org_raw - box_center;
    float3 plane_t = -sign(dir) * box_extent;

    float3 ts = (plane_t - org) / dir;
    float tres = 1e10;
    if (ts.x > 0 && all(abs((org + dir * ts.x).yz) < box_extent.yz)) {
        tres = min(tres, ts.x);
    }
    if (ts.y > 0 && all(abs((org + dir * ts.y).zx) < box_extent.zx)) {
        tres = min(tres, ts.y);
    }
    if (ts.z > 0 && all(abs((org + dir * ts.z).xy) < box_extent.xy)) {
        tres = min(tres, ts.z);
    }
    
    if (tres < 1e9) {
        pos = org_raw + dir * tres;
        return true;
    }
    return false;
}

// Convert incoming light direction to outgoing light direction.
// Sets transmittance to [0, 1] (0 = total internal refraction. retutrned )
// Precondition: dot(dir, normal) < 0, |dir| = 1, |normal| = 1
// refr_index > 1 when entering denser medium.
//
// cf.
// https://en.wikipedia.org/wiki/Snell%27s_law
// https://en.wikipedia.org/wiki/Fresnel_equations
float refract(float3 dir, float3 normal, float refr_index, out float3 trans_dir, out float3 refl_dir) {
    float cos_in = -dot(dir, normal);
    float3 dir_proj = -cos_in * normal;
    float3 dir_perp = dir - dir_proj;  // = normalize(perp) * sin_in
    refl_dir = -dir_proj + dir_perp;

    float theta_in = acos(cos_in);
    float sin_out = sin(theta_in) / refr_index;
    if (sin_out >= 1) {
        trans_dir = dir;  // set some resonable value in case result gets used accidentally.
        return 0;  // total internal reflection.
    }

    // Compute transmission dir.
    float cos_out = sqrt(1 - pow(sin_out, 2));
    trans_dir = -normal * cos_out + dir_perp / refr_index;

    // Fresenel equation.
    float refl_s = pow((cos_in - refr_index * cos_out) / (cos_in + refr_index * cos_out), 2);
    float refl_p = pow((cos_out - refr_index * cos_in) / (cos_out + refr_index * cos_in), 2);
    float refl = (refl_s + refl_p) * 0.5;
    return 1 - refl;
}
